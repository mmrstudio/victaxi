<?php

    /* Template Name: Page - Home */

    get_header();




?>


    <section class="slider">

          <div class="main-carousel">
          <?php

                // check if the repeater field has rows of data
                if( have_rows('slider') ):

                // loop through the rows of data
                  while ( have_rows('slider') ) : the_row();

                      // display a sub field value
                      $slider_image = get_sub_field('image');


                      $slider_image_url = $slider_image['url']; ?>

                      <div class="single_slider">
                          <div class="single_slider__image" style="background-image:url(<?php echo $slider_image_url; ?>);">
                          </div>
                     </div>
                  <?php endwhile;

                else :

                  // no rows found

                endif;

          ?>
        </div>
        <div class="slider_info">

          <?php

                // check if the repeater field has rows of data
                if( have_rows('slider') ):
                  $i = 0;
                // loop through the rows of data
                  while ( have_rows('slider') ) : the_row(); ?>


                      <div class="single_slider_info index<?php echo $i; ?>" >

                            <div class="slider_info">
                              <div class="black_box">
                                  <div class="black_box__inner">
                                      <h1 class="title"><?php the_sub_field('title'); ?></h1>
                                      <?php the_sub_field('discription'); ?>
                                      <?php if (get_sub_field('button_text')): ?>
                                          <a href="<?php the_sub_field('button_url'); ?>" class="button white"><?php the_sub_field('button_text'); ?></a>
                                      <?php endif; ?>
                                  </div>
                              </div>
                          </div>
                     </div>
                  <?php $i ++;endwhile;

                else :

                  // no rows found

                endif;

          ?>

    		</div>


        <div class="filter_box box1"></div>
        <div class="filter_box box2"></div>
        <div class="filter_box box3"></div>
        <div class="filter_box box4"></div>

		</section>







    <section class="home-story wrapper">
          <div class="wrapper__inner">
            <div class="content_narrow">
              <h1 class="title"><?php the_field('home_content_title'); ?></h1>
              <?php the_field('home_content_discription'); ?>
              <a href="<?php the_field('home_content_button_url'); ?>" class="button"><?php the_field('home_content_button_text'); ?></a>
            </div>
          </div>
    </section>


    <section class="home-lastest-news wrapper grey">
      <div class="wrapper__inner">
         <h1 class="title">Latest news</h1>


          <div class="news_wrapper">
          <?php $loop = new WP_Query( array( 'post_type' => 'newsarticle', 'posts_per_page' => -1 ) ); ?>
          <?php $i = 0; while ( $loop->have_posts() ) : $loop->the_post(); ?>

          <?php if(get_field('show_on_home_page')): $i++; ?>

          <div class="single_news">
              <div class="news_image">
                  <a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) {the_post_thumbnail('thumb');} ?></a>
              </div>
              <div class="news_title">
                  <a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a>
              </div>
              <div class="news_date">
                  <?php echo get_the_date('d M Y'); ?>
              </div>
              <div class="news_content">
                  <?php echo get_the_excerpt().'... <a href="'.get_the_permalink().'" class="readmore">Read more</a>'; ?>
              </div>
          </div>
          <?php endif; ?>
          <?php endwhile; wp_reset_query(); ?>


          <?php if($i < 4): $rest_news_num = 4-$i; ?>
              <?php $loop = new WP_Query( array( 'post_type' => 'newsarticle', 'posts_per_page' => $rest_news_num ) ); ?>
              <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>



              <div class="single_news">
                  <div class="news_image">
                      <a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) {the_post_thumbnail('thumb');} ?></a>
                  </div>
                  <div class="news_title">
                      <a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a>
                  </div>
                  <div class="news_date">
                      <?php echo get_the_date('d M Y'); ?>
                  </div>
                  <div class="news_content">
                      <?php echo get_the_excerpt().'... <a href="'.get_the_permalink().'" class="readmore">Read more</a>'; ?>
                  </div>
              </div>

              <?php endwhile; wp_reset_query(); ?>
          <?php endif; ?>
        </div>


            <div class="view_all_bt"><a href="/news" class="button">View All</a></div>
        </div>
    </section>








<?php


    get_footer();

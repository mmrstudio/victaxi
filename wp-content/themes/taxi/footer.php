

  <section class="footer home-story wrapper">
      <div class="gototop">go to top</div>
      <div class="wrapper__inner">
          <?php if(is_page(array( 81, 102, 104, 106, 140, 143, 155, 159 ))) : ?>
          <div class="sign_up_form">
              <div class="title">Sign up for the newsletter</div>
        	       <?php //echo do_shortcode('[gravityform id=1 title=false description=false ajax=true tabindex=49]'); ?>
                 <div class="gf_browser_chrome gform_wrapper" id="gform_wrapper_1">
                       <form action="https://victoriantaxiassocaition.createsend.com/t/d/s/alyxr/" method="post" id="subForm">
                          <div class="gform_body">
                            <ul>
                                <li class="gfield">
                                    <label for="fieldName" class="gfield_label">Name</label>
                                    <div class="ginput_container ginput_container_text">
                                        <input id="fieldName" name="cm-name" type="text" placeholder="Name"/>
                                    </div>

                                </li>
                                <li class="gfield">
                                    <label for="fieldEmail" class="gfield_label">Email</label>
                                    <div class="ginput_container ginput_container_email">
                                          <input id="fieldEmail" name="cm-alyxr-alyxr" placeholder="Email" type="email" required />
                                    </div>

                                </li>
                            </ul>
                          </div>
                          <div class="gform_footer top_label">
                                <input type="submit" class="gform_button button" value="Subscribe">
                          </div>
                      </form>

                 </div>

         </div>
       <?php endif; ?>
         <div class="copy">Copyright © 2017 Taxi Business Council of Australia. All rights reserved.      &nbsp;&nbsp;| &nbsp;&nbsp;     <a href="#">Terms of Use</a>      &nbsp;&nbsp;| &nbsp;&nbsp;      <a href="#"> Privacy Policy</a>     &nbsp;&nbsp;| &nbsp;&nbsp;      <a href="http://mmr.com.au" target="_blank">Site by MMR</a></div>
      </div>
 </secton>

<?php wp_footer(); ?>

    </body>

</html>

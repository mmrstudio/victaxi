<?php
	/**
 * Template Name: News archive page
 */
    get_header();

    if ( have_posts() ) while ( have_posts() ) : the_post(); // start loop



?>


<section class="wrapper first">
		<div class="wrapper__inner" >
				<h1 class="title"><?php the_title(); ?></h1>



					<?php
					if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
					elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
					else { $paged = 1; }

          //echo($paged);


         if( isset($_POST['searchnews'])):


              $keyword = $_POST['keywords'];
              $type = $_POST['type'];
              $state = $_POST['state'];
              $yearmonth = intval($_POST['month']);
              $tags = $_POST['tags'];

              //var_dump ($month);


              $tax_query =  array(
                    'relation' => 'AND',
              );


              if($type!='all') {

                $tax_query[] = array(
                  'taxonomy' => 'news_types',
                  'field'    => 'slug',
                  'terms'    => $type,
                );

              }

              if($state!='all') {

                $tax_query[] = array(
                  'taxonomy' => 'news_state',
                  'field'    => 'slug',
                  'terms'    => $state,
                );

              }

              //var_dump (    $tags );
              if($tags) {

                $tax_query[] = array(
                  'taxonomy' => 'tags',
                  'field'    => 'slug',
                  'terms'    => $tags,
                );

              }

              //var_dump ($tax_query);


              $arg = array(

                'post_status' => 'publish',
                'post_type' => 'newsarticle',
                'posts_per_page' => 6,
                'paged'          => $paged,
                's' =>   $keyword,
                'tax_query' =>  $tax_query,
                'm' => $yearmonth
              );



          else:

    					$arg = array(
    						'post_status' => 'publish',
    						'post_type' => 'newsarticle',
    						'posts_per_page' => 6,
    						'paged'          => $paged

    					);

              //var_dump ($arg );
          endif;
          //var_dump (  $arg);


          $get_latest_news = new WP_Query ($arg); ?>



          <div class="news-filter">
              <h3>Filter media</h3>

              <?php // Get a list of types and extract their names
              $post_types = get_terms(  'news_types', array(
                            'hide_empty' => false,
                        )  );

              //var_dump ($post_types);

              $states = get_terms( 'news_state' , array(
                    'hide_empty' => false,
                ) );
              // Get a list of tags and extract their names
              $tags = get_terms( 'tags' , array(
                    'hide_empty' => false,
                ) );

              //var_dump($tags);

              ?>

              <form class="search" action="" method="post">
                  <ul class="filter_list">
                        <li>
                          <label for="keywords">Search</label>
                          <input type="text" name="keywords" placeholder="Enter Keywords">
                       </li>
                       <li>
                         <label for="type">Type</label>
                         <select name="type">
                            <option value="all">All</option>
                            <?php foreach ($post_types as $type) {
                                echo '<option value="'.$type->slug.'">'.$type->name.'</option>';
                            }  ?>
                          </select>
                        </li>
                        <li>
                          <label for="state">State</label>
                          <select name="state">
                             <option value="all">All</option>
                             <?php foreach ($states as $state) {
                                 echo '<option value="'.$state->slug.'">'.$state->name.'</option>';
                             }  ?>
                           </select>
                         </li>
                        <li>
                          <label for="month">Month</label>
                          <select name="month">
                             <option value="all">All</option>
                             <?php
                                for ($i=0; $i<=12; $i++) {
                                echo '<option value="'.date('Ym', strtotime("-$i month")).'">'.date('F Y', strtotime("-$i month")).'</option>';
                                 }
                              ?>
                          </select>
                       </li>
                       <li>
                         <label for="tags">Related tags</label>

                         <?php foreach ($tags as $tag) {
                             echo '<div class="checkbox"><input type="checkbox" name="tags[]" value="'.$tag->slug.'" id="'.$tag->slug.'"><label class="checkbox_label" for="'.$tag->slug.'" >'.$tag->name.'</label></div>';
                         }  ?>


                      </li>
                      <li>
                         <input type="submit" name="searchnews" class="button" value="search">
                     </li>
                  </ul>
              </form>

          </div>


				 <?php	if ( $get_latest_news->have_posts() ) : ?>
            <div class="news-summary">
         <?php  while ( $get_latest_news->have_posts() ) : $get_latest_news->the_post(); ?>


							<div class="newsbody">

    								<div class="newsbody__left">
    									    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumb'); ?></a>
    								</div>

    								<div class="newsbody__right">
                          <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>

                          <h4><?php echo get_the_date('d M Y'); ?></h4>
        									<?php echo get_the_excerpt().'... <a href="'.get_the_permalink().'" class="readmore">Read more</a>'; ?>

    								</div>

							</div>

					<?php endwhile;wp_reset_query(); ?>
              </div>
         <?php endif; ?>





					<div class="news-pagination">
						<?php echo custom_pagination($get_latest_news->max_num_pages,8,$paged); ?>
					</div>
					<?php wp_reset_query(); ?>
			</div>

</section>

<?php

        get_footer();

    endwhile; // end the loop

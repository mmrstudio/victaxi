<?php
	/* Template Name: Page - About our team*/
      get_header();

      if(have_posts()) while (have_posts()) : the_post();

?>

<section class="wrapper first about-story">
   <div class="wrapper__inner" >
       <div class="content_narrow">
           <h1 class="title">About us</h1>
           <?php $the_query = new WP_Query( 'page_id=100' );

           while ($the_query -> have_posts()) : $the_query -> the_post();

                 the_content();

           endwhile; wp_reset_query(); ?>
      </div>
   </div>
</section>



<div class="main_content">
    <section class="wrapper">
      <div class="wrapper__inner" >
          <div class="side_menu">
              <ul>
                  <?php $the_query = new WP_Query(
                     array(
                      'post_type'      => 'page',
                      'posts_per_page' => -1,
                      'post_parent' => 100,
                      'orderby'        => 'menu_order'
                    )
                  );

                  while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

                        <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>

                <?php  endwhile; wp_reset_query(); ?>
            </ul>
          </div>

          <div class="side_menu_right">
              <h2><?php the_title(); ?></h2>

              <h3 class="type">Board</h3>
              <div class="team_members board">
              <?php
                    // check if the repeater field has rows of data
                    if( have_rows('team_member') ):

                    // loop through the rows of data
                      while ( have_rows('team_member') ) : the_row();

                          // display a sub field value


                          $member_type = get_sub_field('board__staff');

                          if($member_type =='board'):

                                $member_image = get_sub_field('photo');

                                $member_image_url =   $member_image['url'];

                                echo '<div class="team_members__single">

                                      <img src="'.$member_image_url.'">

                                      <div class="team_members__name"><h3>'.get_sub_field('name').'</h3></div>

                                      <div class="team_members__title"><h4>'.get_sub_field('job_title').'</h4></div>

                                      <div class="team_members__discription">'.get_sub_field('discription').'</div>

                                </div>';
                          endif;
                      endwhile;

                    else :

                      // no rows found

                    endif;

              ?>
            </div>


              <h3 class="type">Staff</h3>
              <div class="team_members staff">
              <?php
                    // check if the repeater field has rows of data
                    if( have_rows('team_member') ):

                    // loop through the rows of data
                      while ( have_rows('team_member') ) : the_row();
                          $member_type = get_sub_field('board__staff');

                          if($member_type =='staff'):
                          // display a sub field value
                                $member_image = get_sub_field('photo');


                                  $member_image_url =   $member_image['url'];

                                  echo '<div class="team_members__single">

                                        <img src="'.$member_image_url.'">

                                        <div class="team_members__name"><h3>'.get_sub_field('name').'</h3></div>

                                        <div class="team_members__title"><h4>'.get_sub_field('job_title').'</h4></div>

                                        <div class="team_members__discription">'.get_sub_field('discription').'</div>

                                  </div>';
                          endif;
                      endwhile;

                    else :

                      // no rows found

                    endif;

              ?>
            </div>
        </div>

      </div>
    </section>


  </div>




<?php

endwhile; // end loop


get_footer();

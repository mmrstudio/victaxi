<?php
	/* Template Name: Page - passengers*/

    get_header();

        if(have_posts()) while (have_posts()) : the_post();



?>

<section class="wrapper first passengers">
  <div class="wrapper__inner" >
      <h1 class="title"><?php the_title(); ?></h1>
      <div class="content">
        <?php the_content(); ?>
        <a href="<?php the_field('button_url'); ?>" class="button large"><?php the_field('button_text'); ?></a>
      </div>
      <div class="thumbnail">
          <?php the_post_thumbnail(); ?>
      </div>

  </div>
</section>


<?php

    endwhile; // end loop

    get_footer();

<?php
	/* Template Name: Page -what we stand for policy*/
      get_header();

      if(have_posts()) while (have_posts()) : the_post();

?>

<section class="wrapper first">
   <div class="wrapper__inner" >

          <h1 class="title">What we stand for</h1>

          <div class="side_menu">
              <ul>
                  <?php

                     $partent_id = wp_get_post_parent_id (get_the_ID());
                     $the_query = new WP_Query(
                     array(
                      'post_type'      => 'page',
                      'posts_per_page' => -1,
                      'post_parent' => $partent_id,
                      'orderby'        => 'menu_order'
                    )
                  );

                  while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

                        <li><a href="<?php the_permalink(); ?>" class="large" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>

                <?php  endwhile; wp_reset_query(); ?>
            </ul>
          </div>

          <div class="side_menu_right">
              <?php the_content(); ?>
          </div>

  </div>
</section>



<?php

endwhile; // end loop


get_footer();

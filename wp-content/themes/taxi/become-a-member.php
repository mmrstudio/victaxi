<?php
	/* Template Name: Page become a member*/
      get_header();

      if(have_posts()) while (have_posts()) : the_post();

?>

<section class="wrapper first">
   <div class="wrapper__inner" >

          <h1 class="title">Membership</h1>

          <div class="side_menu">
              <ul>
                  <?php

                     $partent_id = wp_get_post_parent_id (get_the_ID());
                     $the_query = new WP_Query(
                     array(
                      'post_type'      => 'page',
                      'posts_per_page' => -1,
                      'post_parent' => 159,
                      'orderby'        => 'menu_order'
                    )
                  );

                  while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

                        <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>

                <?php  endwhile; wp_reset_query(); ?>
            </ul>
          </div>


          <div class="side_menu_right">
                <div class="left content">
                  <?php the_content(); ?>
                </div>
                 <div class="right">
                    <?php the_post_thumbnail(); ?>

                </div>
          </div>

  </div>
</section>



<?php

endwhile; // end loop


get_footer();

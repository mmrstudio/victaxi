<?php
	/* Template Name: Page - About structure*/
     get_header();

    if(have_posts()) while (have_posts()) : the_post();


?>

    <section class="wrapper first about-story">
       <div class="wrapper__inner" >
           <div class="content_narrow">
               <h1 class="title">About us</h1>
               <?php $the_query = new WP_Query( 'page_id=100' );

               while ($the_query -> have_posts()) : $the_query -> the_post();

                     the_content();

               endwhile; wp_reset_query(); ?>
          </div>
       </div>
    </section>


    <div class="main_content">
        <section class="wrapper">
          <div class="wrapper__inner" >
              <div class="side_menu">
                  <ul>
                      <?php $the_query = new WP_Query(
                         array(
                          'post_type'      => 'page',
                          'posts_per_page' => -1,
                          'post_parent' => 100,
                          'orderby'        => 'menu_order'
                        )
                      );

                      while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

                            <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>

                    <?php  endwhile; wp_reset_query(); ?>
                  </ul>
            </div>

            <div class="side_menu_right">
                <div class="mapleft">
              				<h2><?php the_title(); ?></h2>
                      <h4><?php the_field('sub_heading'); ?></h4>
                      <?php the_field('discription'); ?>


                          <?php
                              $map =  get_field('map');
                                //var_dump ();
                          ?>

                          <script>
                              window.svgmap = <?php echo json_encode($map); ?>;
                              //alert(JSON.stringify(window.map));
                          </script>

                </div>
                <div class="state_info">

                </div>
                 <div class="svgmap">
                   <?xml version="1.0" encoding="utf-8"?>
                   <!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
                   <svg version="1.1" id="svgMap" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                   	 viewBox="0 0 459.4 430.6" style="enable-background:new 0 0 459.4 430.6;" xml:space="preserve">
                   <style type="text/css">

                   	.st1{fill:#FFFFFF;stroke:#E76820;stroke-miterlimit:10;}
                   	.st2{ font-family: 'Museo-700', Helvetica, Arial, sans-serif;font-weight: normal;  pointer-events: none;}
                   	.st3{font-size:16px;}
                   	.st4{fill:#231F20;}
                   	.st5{letter-spacing:-1;}
                   </style>
                   <title>map</title>
                   <path class="st1 state tas" data-number="TAS" data-text="TAS" data-name="Tasmania" d="M357.2,378.5c5.4-3.2,18,9.4,21.9,8.6s12.5-5.5,17.2-3.9s0,29.7-2.4,31.3s-3.9,0-6.3,2.4s-3.9,7.8-10.2,7.8
                   	s-21.9-29-22.7-34.4S353.3,380.8,357.2,378.5z"/>
                   <path class="st1 state qld" data-number="QLD" data-text="QLD" data-name="Queensland" d="M440.9,167.3c-5.5-10.2-44.6-50.8-54.8-61s-11.7-14.1-14.9-25.8s-5.5-35.2-11-39.9s-10.2,0-12.5-2.3
                   	s-10.1-29-12.5-34.4s-7-3.9-8.6,3.9s-13.3,60.2-14.9,65.7s-9.4,4.7-14.1,1.6c-1.9-1.3-8.8-6-16.4-11.1v114.4h33.6v39.1H406l5-4.7
                   	c1.1-0.9,2.6-1.4,4-1.4l10.2,1.1c1.5,0.2,2.8,1,3.8,2.1l2.6,3.3c0.9,1,2.3,1.4,3.5,0.8l3.6-1.8c1.2-0.7,2-2,2.1-3.4
                   	c0.1-1.4,0.8-2.7,2-3.5l2.9-1.6c1.3-0.6,2.8-0.8,4.3-0.3l1.9,0.7c1.4,0.5,3,0.6,4.5,0.3l1.5-0.4C455,193,445.6,176.1,440.9,167.3z"
                   	/>
                   <path class="st1 state nt" data-number="NT" data-text="NT" data-name="Northern Territory" d="M281.3,63.8c-11-7.5-23.6-16.1-25-17c-2.4-1.6-3.9-7,0-12.5s11-16.4,11-16.4s1.6-6.3-3.9-7s-28.2-1.6-33.6-3.1
                   	s-8-6-13.3,4.7c-2.4,4.7-12.5,0-16.4,1.6s-20.3,26.6-20.3,26.6c-0.2,0.4-0.5,0.7-0.8,1v136.7h102.4V63.8z"/>
                   <path class="st1 state wa" data-number="WA" data-text="WA" data-name="Western Australia"  d="M178.8,41.5c-1.1,1.1-3.4,2.4-7-0.3c-5.5-3.9-13.3-10.2-13.3-10.2s-2.4-4.7-10.2,3.1s-35.2,27.4-40.7,36
                   	s-9.4,21.1-9.4,21.1s-9.4,12.5-16.4,14.9s-61.8,18-65.7,21.9C12.2,132,3.5,169.6,2,175.1s-2.3,7,0,11s25.8,63.3,25.8,68.8
                   	s-7,16.4-9.4,21.1s0.8,9.4,9.4,15.6s13.3,10.2,21.9,7.8s18-14.1,27.4-15.6s27.4-1.6,34.4-1.6s9.4-2.4,13.3-7.8s20.3-14.1,30.5-15.6
                   	c4.8-0.7,13.8-2.9,23.5-4.8V41.5z"/>
                   <path class="st1 state vic" data-number="VIC" data-text="VIC" data-name="Victoria"  d="M398.4,324.7c-1.1-0.4-1.9-1.4-2.1-2.5c-0.1-1.3-0.4-2.6-0.8-3.8l-1.5-4.2c-0.5-1.3-1.8-2.1-3.1-2l-4.7,0.4
                   	c-1.6,0.1-3.1,0.1-4.7,0l-6.3-0.5c-1.5-0.2-3-0.7-4.3-1.5l-0.8-0.5c-1.3-0.7-2.8-1-4.2-0.7l-8,2c-1.4,0.3-2.8-0.2-3.8-1.2L340.7,295
                   	c-1-1-2.4-1.4-3.8-1.1l-1,0.3c-1.3,0.3-2.7-0.3-3.3-1.5l-2.6-5.2c-0.7-1.2-2-1.9-3.4-1.7l-4,0.7c-1.5,0.2-3,0-4.4-0.7l-3.4-1.9v54.3
                   	c8.4,4.9,17.9,8.4,21.1,9.7c5.5,2.4,8.6,2.4,13.3,0s6.3-4.7,11.7-3.9s7.8,8.6,14.1,7.8s9.4-12.5,16.4-13.3s17.2,3.1,21.1-4.7
                   	c0.3-0.6,0.6-1.3,1-2.2L398.4,324.7z"/>
                   <path class="st1 state sa" data-number="SA" data-text="SA" data-name="South Australia" d="M314.9,178.2H178.8V254c10.9-2.1,22.7-3.9,30.5-3.1c14.9,1.6,27.4,10.2,35.2,24.2s18,17.2,26.6,14.1
                   	s8.6-5.5,14.1,3.1s11.7,22.7,16.4,33.6c2,4.6,7.3,8.7,13.3,12.2L314.9,178.2z"/>
                   <path class="st1 state nsw" data-number="NSW"  data-text="NSW" data-name="New South Wales" d="M458.9,215.8c-0.1-2.4-0.4-4.8-0.9-7.1l-1.5,0.4c-1.5,0.3-3,0.2-4.5-0.3l-1.9-0.7c-1.4-0.4-2.9-0.3-4.3,0.3
                   	l-2.9,1.6c-1.2,0.8-2,2.1-2,3.5c-0.1,1.4-0.9,2.7-2.1,3.4l-3.6,1.8c-1.2,0.6-2.7,0.2-3.5-0.8l-2.6-3.3c-1-1.1-2.3-1.9-3.8-2.1
                   	l-10.2-1.1c-1.5-0.1-2.9,0.4-4,1.4l-5,4.7h-91V284l3.4,1.9c1.4,0.7,2.9,0.9,4.4,0.7l4-0.7c1.4-0.2,2.7,0.5,3.4,1.7l2.6,5.2
                   	c0.7,1.2,2,1.8,3.3,1.5l1-0.3c1.4-0.3,2.8,0.1,3.8,1.1l13.3,15.2c1,1,2.4,1.5,3.8,1.2l8-2c1.5-0.3,3,0,4.2,0.7l0.8,0.5
                   	c1.3,0.8,2.8,1.3,4.3,1.5l6.3,0.5c1.6,0.1,3.1,0.1,4.7,0l4.7-0.4c1.4-0.1,2.6,0.7,3.1,2l1.5,4.2c0.4,1.2,0.7,2.5,0.8,3.8
                   	c0.2,1.2,1,2.1,2.1,2.5l15.3,7l0,0c4.4-11.2,13.5-45.4,18.6-53.3C437.8,269.7,459.6,233,458.9,215.8z"/>
                   <polygon id="Australian_Capital_Territory" class="st1 state act" data-number="ACT" data-text="ACT" data-name="Australian Capital Territory" points="410.9,297.2 410.3,296.9 410,296.8 409.9,296.8 409.8,296.8
                   	409.1,296.5 409,296.5 408.9,296.4 408.9,296.3 409,296.2 409,296.2 408.9,296.1 408.9,296 408.8,296 408.6,296 408.5,296
                   	408.4,295.9 408.3,295.8 408.3,295.8 408.3,295.7 408.3,295.7 408.3,295.5 408.3,295.5 408.2,295.4 408.2,295.3 408.1,295.3
                   	407.5,294.9 407.1,295 405.1,296.3 403.6,297.1 403.5,297.2 403.4,297.3 403.4,297.4 403.3,297.5 403.3,297.6 403.3,297.6
                   	403.4,297.7 403.4,297.8 403.4,297.9 403.4,298 403.2,298.4 403.1,298.5 403.1,298.8 402.9,299.4 402.9,299.5 403,300.1
                   	403.1,300.5 403.1,300.6 403.1,300.8 403,301.2 403,301.4 403,301.5 403.2,301.6 403.2,301.6 403.2,301.7 403.2,301.9 403.2,302
                   	403.2,302.1 403.3,302.1 403.3,302.2 403.7,302.5 403.8,302.6 403.9,302.6 403.9,302.6 404,302.6 404,302.6 404.1,302.4
                   	404.1,302.4 404.2,302.4 404.3,302.3 404.4,302.3 404.4,302.4 404.5,302.5 404.5,302.8 404.6,303.5 404.7,303.8 404.9,304.1
                   	405,304.2 405,304.3 405,304.3 405.2,304.4 405.9,304.6 406.2,304.7 406.3,304.8 406.4,304.8 406.5,304.7 406.5,304.7 406.5,304.6
                   	406.6,304.6 406.7,304.4 406.7,304.3 406.8,304.2 406.8,304.2 406.9,304.1 407,304.1 407.1,304 407.1,304 407.1,303.9 407.2,303.8
                   	407.2,303.6 407.2,303.4 407.1,303 407,302.8 407.1,302.7 407.2,302.4 407.2,302.2 407.1,301.8 407,301.1 406.9,300.8 407,300.7
                   	407.1,300.6 407.4,300.5 407.5,300.4 407.5,300.3 407.5,300.2 407.7,299.8 407.8,299.7 407.8,299.6 407.7,299.4 407.7,299.4
                   	407.6,299.3 407.6,299.1 407.6,299.1 407.6,299 407.7,298.9 407.7,298.8 407.7,298.8 407.6,298.7 407.6,298.6 407.7,298.6
                   	407.7,298.5 407.8,298.5 407.9,298.4 408,298.3 408.1,298 408.2,297.9 408.3,297.9 408.4,297.8 408.5,297.8 408.5,297.8
                   	408.7,297.9 408.8,298 408.9,297.9 408.9,297.9 409,297.8 409.2,297.6 409.3,297.5 409.4,297.5 410.3,297.7 410.4,297.6
                   	410.5,297.6 410.7,297.5 410.8,297.4 411,297.3 411,297.3 411,297.2 "/>
                   <text transform="matrix(1 0 0 1 340.6 334.5)" class="st2 st3 vic">VIC</text>
                   <text transform="matrix(1 0 0 1 85.4 188.5)" class="st4 st2 st3 wa">W</text>
                   <text transform="matrix(1 0 0 1 100.0238 188.5)" class="st4 st2 st3 wa"> </text>
                   <text transform="matrix(1 0 0 1 100.78 188.5)" class="st4 st2 st3 wa">A</text>
                   <text transform="matrix(1 0 0 1 219.26 123.5)" class="st4 st2 st3 nt">N </text>
                   <text transform="matrix(1 0 0 1 231.58 123.5)" class="st4 st2 st3 nt">T</text>
                   <text transform="matrix(1 0 0 1 235.7 226.5)" class="st4 st2 st3 sa">SA</text>
                   <text transform="matrix(1 0 0 1 363.16 405.5)" class="st4 st2 st3 st5 tas">T</text>
                   <text transform="matrix(1 0 0 1 371.4959 405.5)" class="st4 st2 st3 tas"> </text>
                   <text transform="matrix(1 0 0 1 372.23 405.5)" class="st4 st2 st3 tas">AS</text>
                   <text transform="matrix(1 0 0 1 387.46 286.5)" class="st4 st2 st3 act">A</text>
                   <text transform="matrix(1 0 0 1 398.2599 286.5)" class="st4 st2 st3"> </text>
                   <text transform="matrix(1 0 0 1 398.04 286.5)" class="st4 st2 st3 act">C</text>
                   <text transform="matrix(1 0 0 1 409.8959 286.5)" class="st4 st2 st3 act"> </text>
                   <text transform="matrix(1 0 0 1 409.38 286.5)" class="st4 st2 st3 act">T</text>
                   <text transform="matrix(1 0 0 1 358.18 255.5)" class="st4 st2 st3 nsw">NS </text>
                   <text transform="matrix(1 0 0 1 379.28 255.5)" class="st4 st2 st3 nsw">W</text>
                   <text transform="matrix(1 0 0 1 336.19 151.5)" class="st4 st2 st3 qld">Q </text>
                   <text transform="matrix(1 0 0 1 348.89 151.5)" class="st4 st2 st3 qld">L</text>
                   <text transform="matrix(1 0 0 1 357.8978 151.5)" class="st4 st2 st3"> </text>
                   <text transform="matrix(1 0 0 1 357.33 151.5)" class="st4 st2 st3 qld">D</text>
                   </svg>

            </div>
          </div>
        </section>



        <section class="wrapper grey">
          <div class="wrapper__inner" >

                <div class="members">
                     <h2>Members</h2>
                <?php


                  // check if the repeater field has rows of data
                  if( have_rows('members') ):?>
                  <ul class="member_list">
                  <?php  while ( have_rows('members') ) : the_row();

                        $image = get_sub_field('thumbnail');

                        $image_url= get_sub_field('link_url');

                        if($image_url): ?>

                            <li><a href="http://<?php echo $image_url; ?>" target="_blank"><img src ="<?php echo $image['url']; ?> "></a></li>

                       <?php else: ?>
                            <li><img src ="<?php echo $image['url']; ?> "></li>
                       <?php endif;  ?>
                  <?php endwhile;

                  else :   // no rows found ?>

                  </ul>
                  <?php endif; ?>

                </div>
          </div>
        </section>


   </div>


<?php

    endwhile; // end loop


    get_footer();

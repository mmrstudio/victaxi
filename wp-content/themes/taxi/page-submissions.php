<?php
	/**
 * Template Name: Page - Submissions
 */
    get_header();

    if ( have_posts() ) while ( have_posts() ) : the_post(); // start loop




    					if ( get_query_var('paged') ) { $paged = get_query_var('paged'); }
    					elseif ( get_query_var('page') ) { $paged = get_query_var('page'); }
    					else { $paged = 1; }


             if( isset($_POST['searchdocs'])):
                  //die("22222222");


                  $keyword = $_POST['keywords'];
                  $group = $_POST['group'];
                  $tags = $_POST['tags'];


                  $tax_query =  array(
                        'relation' => 'AND',
                  );


                  if($group!='all') {

                    $tax_query[] = array(
                      'taxonomy' => 'group',
                      'field'    => 'slug',
                      'terms'    => $group,
                    );

                  }

                  //var_dump (    $tags );
                  if($tags) {

                    $tax_query[] = array(
                      'taxonomy' => 'doctags',
                      'field'    => 'slug',
                      'terms'    => $tags,
                    );

                  }

                  var_dump ($tax_query);


                  $arg = array(

                    'post_status' => 'publish',
                    'post_type' => 'submission',
                    'posts_per_page' => -1,
                    'paged'          => $paged,
                    's' =>   $keyword,
                    'tax_query' =>  $tax_query,
                  );



              else:

        					$arg = array(
        						'post_status' => 'publish',
        						'post_type' => 'submission',
        						'posts_per_page' => -1,
        						'paged'          => $paged

        					);
              endif;
              //var_dump (  $arg);


              $get_latest_news = new WP_Query ($arg); ?>







<section class="wrapper first">
		<div class="wrapper__inner" >
      <h1 class="title">What we stand for</h1>

          <div class="side_menu">
              <ul>
                  <?php

                     $partent_id = wp_get_post_parent_id (get_the_ID());
                     $the_query = new WP_Query(
                     array(
                      'post_type'      => 'page',
                      'posts_per_page' => -1,
                      'post_parent' => $partent_id,
                      'orderby'        => 'menu_order'
                    )
                  );

                  while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

                        <li><a href="<?php the_permalink(); ?>" class="large" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>

                <?php  endwhile; wp_reset_query(); ?>
            </ul>
          </div>

          <div class="side_menu_right submissions">
              <div class="submission-filter">
                    <h2>Submissions</h2>

                    <?php // Get a list of types and extract their names
                    $post_types = get_terms(  'group', array(
                                  'hide_empty' => false,
                    )  );
                    //var_dump ($post_types);


                    // Get a list of tags and extract their names
                    $tags = get_terms( 'doctags' , array(
                                  'hide_empty' => false,
                    )  );

                    //var_dump($tags);

                    ?>

                    <form class="search" action="" method="post">
                        <ul class="filter_list">
                              <li class="keywords">
                                <label for="keywords">Search</label>
                                <input type="text" name="keywords" placeholder="Enter Keywords">
                             </li>
                             <li class="group">
                               <label for="group">Group</label>
                               <select name="group">
                                  <option value="all">All</option>
                                  <?php foreach ($post_types as $type) {
                                      echo '<option value="'.$type->slug.'">'.$type->name.'</option>';
                                  }  ?>
                                </select>
                              </li>
                              <li class="submitbutton">
                                 <input type="submit" name="searchdocs" class="button" value="search">
                             </li>
                             <li class="tags">
                               <label for="tags">Related tags</label>
                               <div class="checkbox_group">
                                   <?php foreach ($tags as $tag) {
                                       echo '<div class="checkbox"><input type="checkbox" name="tags[]" value="'.$tag->slug.'" id="'.$tag->slug.'"><label class="checkbox_label" for="'.$tag->slug.'" >'.$tag->name.'</label></div>';
                                   }  ?>

                              </div>
                            </li>

                        </ul>
                    </form>

                </div>

               <?php  if ( $get_latest_news->have_posts() ) : ?>
                  <div class="submission-summary">
               <?php  while ( $get_latest_news->have_posts() ) : $get_latest_news->the_post(); ?>


                    <div class="single_submission">

                        <div class="single_submission__image">
                            <?php $file = get_field('upload_document_');
                            if( $file ): ?>

                            	<a href="<?php echo $file['url']; ?>" target="_blank"><?php the_post_thumbnail('docs'); ?></a>



                            <?php endif; ?>


                            <div class="single_submission__info">

                                <h4><?php echo get_the_date('d M Y'); ?></h4>
                                <?php echo get_field('description'); ?>
                            </div>

                        </div>

                        <a href="<?php echo $file['url']; ?>" target="_blank"><h3><?php the_title(); ?></h3></a>



                    </div>

                <?php endwhile;wp_reset_query(); ?>
                    </div>
               <?php endif; ?>






                <div class="news-pagination">
                  <?php echo custom_pagination($get_latest_news->max_num_pages,8,$paged); ?>
                </div>
                <?php wp_reset_query(); ?>
          </div>






			</div>

</section>

<?php

        get_footer();

    endwhile; // end the loop

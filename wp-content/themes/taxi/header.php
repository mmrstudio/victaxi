<!DOCTYPE HTML>

<html lang="en"xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="http://www.facebook.com/2008/fbml">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <?php global $meta_tags; echo isset($meta_tags) ? $meta_tags : ''; ?>

       <!-- <title><?php echo wp_title(); ?></title>-->
		     <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        
        <!-- typekit font Museo-->
        <script src="https://use.typekit.net/bjl8bxj.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>

        <script>
            var siteURL = '<?php echo site_url(); ?>/';
            var wp_ajax_url = '<?php echo site_url('/wp-admin/admin-ajax.php'); ?>';
            var themeURL = '<?php echo THEME_URL; ?>';
            var ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
        </script>


        <?php if(is_page('current-members')) : ?>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places,geometry&key=AIzaSyCQwWlMbs4g9YlXT8dRgDQM4hFWwFC4xX4"></script>
        <?php endif; ?>


        <?php wp_head(); ?>

        <?php /*
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        */ ?>

        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

          //ga('create', 'UA-5333835-52', 'auto');
          ga('send', 'pageview');

        </script>


        <link href='https://fonts.googleapis.com/css?family=Merriweather:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

    </head>

    <body <?php body_class(); ?>>

        <div class="body-wrap" id="top">

        <div class="memberlogin wrapper ">
    			<div class="wrapper__inner">
    				 <div class="form">

    					 <form id="login" action="login" method="post">
                 <div class="form_top"></div>
    					 	<div class="loginclose"><span>X</span></div>


    						<h3>Log in below to access<br> the Members Area</h3>
    						<p class="status"></p>
    						<ul>
    							<li><input id="username" type="text" name="username" placeholder="Username"></li>

    							<li><input id="password" type="password" name="password"  placeholder="Password"></li>

    							<li><button class="submit_button button" type="submit" value="Login" name="submit">Login</button></li>
    						</ul>
    						<a class="lost" href="<?php echo wp_lostpassword_url(); ?>">Forgot your password/username?</a>


    						<?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
    					</form>
    				</div>
    			</div>
    	</div>




        <header class="header">
              <div class="responsive-nav" id="responsiveNav">

                  <nav class="responsive-nav--menu">
                      <?php wp_nav_menu( array('echo'=>true) ); ?>

                      <ul class="side_menus_hide">
                          <li>
                              <a href="#" class="login icon-member">Member login</a>
                          </li>
                          <li>
                              <a href="#" class="linkedin icon-linkedin">linkedin</a>
                          </li>
                      </ul>
                  </nav>



                  <a class="nav-toggle" id="navToggle">Menu</a>



              </div>


              <div class="header__inner">
                  <div class="site-logo">
                    <a href="<?php echo site_url(); ?>"><?php echo get_bloginfo('name'); ?></a>
                  </div>

                  <ul class="side_menus">
                      <li>
                          <a href="#" class="login icon-member">Member login</a>
                      </li>
                      <li>
                          <a href="#" class="linkedin icon-linkedin">linkedin</a>
                      </li>
                  </ul>
              </div>


        </header>

<?php

    get_header();

    if ( have_posts() ) while ( have_posts() ) : the_post(); // start loop



?>


<section class="wrapper first">
		<div class="wrapper__inner single_news" >
        <a href="#" class="goback">‹  Back</a>
        <div class="single_news__banner">

          <?php the_post_thumbnail(); ?>
        </div>
        <div class="single_news__body">
            <h1 class="title"><?php the_title(); ?></h1>
            <h4><?php echo get_the_date('d M Y'); ?></h4>
            <?php the_content(); ?>
            <div class="share-post">
              <span>Share this article</span>
              <ul class="social-icons">

                <?php $thumb = get_the_post_thumbnail(); ?>

                <li><a href="https://www.linkedin.com/shareArticle?url=<?php echo urlencode(get_the_permalink()); ?>&title=<?php echo urlencode(get_the_title()); ?>" target="_blank" class="icon  icon-linkedin">Linkedin</a></li>

                <li><a  href="https://twitter.com/intent/tweet?url=<?php echo urlencode(get_the_permalink()); ?>&text=<?php echo urlencode(get_the_title()); ?>" target="_blank" class="twitter icon icon-twitter">Twitter</a></li>
                <li><a  href="https://www.facebook.com/sharer.php?u=<?php echo urlencode(get_the_permalink()); ?>" target="_blank" class="linked-in icon icon-facebook">Facebook</a></li>

              </ul>

            </div>
        </div>
        <div class="single_news__related">


            <?php //releted news query

                 $types = get_the_terms( get_the_ID(), 'news_types' );

                 foreach ( $types as $type ) {
                    $type_terms[] = $type->slug;
                 }
                 $tags = get_the_terms( get_the_ID(), 'tags' );

                 foreach ( $tags as $tag ) {
                    $tag_terms[] = $tag->slug;
                 }
                 //var_dump ($type_terms);
                 //var_dump ($tag_terms);


                 $tax_query =  array(
                       'relation' => 'OR',
                 );



                 $tax_query[] = array(
                   'taxonomy' => 'news_types',
                   'field'    => 'slug',
                   'terms'    => $type_terms,
                 );



                 if($tags) {

                   $tax_query[] = array(
                     'taxonomy' => 'tags',
                     'field'    => 'slug',
                     'terms'    => $tag_terms,
                   );

                 }



                 $arg = array(

                   'post_status' => 'publish',
                   'post_type' => 'newsarticle',
                   'posts_per_page' => 3,
                   'paged'          => -1,
                   'post__not_in' => array(get_the_ID()),
                   'tax_query' =>  $tax_query,

                 );

                 //var_dump ( $arg);




             $get_releted_news = new WP_Query ($arg);

            if ( $get_releted_news->have_posts() ) : ?>
            <h4>Related articles</h4>
            <div class="news-summary releted">
            <?php  while ( $get_releted_news->have_posts() ) : $get_releted_news->the_post(); ?>


                 <div class="newsbody">

                       <div class="newsbody__top">
                             <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumb'); ?></a>
                       </div>

                       <div class="newsbody__bottom">
                              <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>

                             <h4><?php echo get_the_date('d M Y'); ?></h4>


                       </div>


                 </div>

             <?php endwhile;wp_reset_query(); ?>
            </div>
          <?php else:
              $arg = array(
                'post_status' => 'publish',
                'post_type' => 'newsarticle',
                'posts_per_page' => 3,
                'post__not_in' => array(get_the_ID()),
                'paged'          => -1

              );
               $get_other_news = new WP_Query ($arg); ?>

               <h4>Other articles</h4>
               <div class="news-summary">
               <?php  while ( $get_other_news->have_posts() ) : $get_other_news->the_post(); ?>


                    <div class="newsbody">

                          <div class="newsbody__top">
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumb'); ?></a>
                          </div>

                          <div class="newsbody__bottom">
                                <h3><?php the_title(); ?></h3>

                                <h4><?php echo get_the_date('d M Y'); ?></h4>


                          </div>

                    </div>

                <?php endwhile;wp_reset_query(); ?>
               </div>
          <?php endif; ?>
        </div>





		</div>
</section>

<?php

        get_footer();

    endwhile; // end the loop

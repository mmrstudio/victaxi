<?php
	/* Template Name: Page - About overview*/
   get_header();

  if(have_posts()) while (have_posts()) : the_post();

?>


         <section class="wrapper first about-story">
        		<div class="wrapper__inner" >
                <div class="content_narrow">
              			<h1 class="title">About us</h1>
                    <?php $the_query = new WP_Query( 'page_id=100' );

                    while ($the_query -> have_posts()) : $the_query -> the_post();

                          the_content();

                    endwhile; wp_reset_query(); ?>
               </div>
        		</div>
      	</section>


          <div class="main_content">

                <section class="wrapper">
            			<div class="wrapper__inner" >
                      <div class="side_menu">
                          <ul>
                              <?php $the_query = new WP_Query(
                                 array(
                                  'post_type'      => 'page',
                                  'posts_per_page' => -1,
                                  'post_parent' => 100,
                                  'orderby'        => 'menu_order'
                                )
                              );

                              while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

                                    <li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>

                            <?php  endwhile; wp_reset_query(); ?>
                          </ul>
                    </div>

                    <div class="side_menu_right">
                          <div class="left">
                    				<h2><?php the_title(); ?></h2>

                            <?php the_field('discription'); ?>
                            <?php if (get_field('button_url')): ?>
                              <a href="<?php the_field('button_url'); ?>" class="button large"><?php the_field('button_text'); ?></a>
                            <?php endif; ?>
                          </div>
                           <div class="right">
                              <?php $image = get_field('image_on_right');  ?>
                              <img src ="<?php echo $image['url']; ?> ">

                          </div>
                    </div>
            			</div>
            		</section>



          </div>

        <?php




        endwhile; // end loop

    get_footer();

<?php

    /* Template Name: Page - News */

    if ( have_posts() ) while ( have_posts() ) : the_post(); // start loop

         // load header
         get_header();

?>
    	<section class="banner">
        	<div class="desktop">
               	<?php   $image  = get_field('desktop_banner', 'option'); ?>           
                <img src=" <?php echo $image['url']; ?>" title="hero image">
            </div>
           
            <div class="mobile_banner">
            	 <?php $mobile_image  = get_field('mobile_banner', 'option'); ?>
            	 <img src=" <?php echo $mobile_image['url']; ?>" title="hero image">
            </div>
        </section>

    	<section class="page-section__white innerpage">
			<div class="page-section__inner" >
				<h4>News</h4>
			</div>		
		</section>

       <section class="page-section__white">

			<div class="page-section__inner" >
                    
				<?php
				$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
				
				$get_latest_news = new WP_Query(array(
					'post_status' => 'publish',
					'post_type' => 'news',
					'posts_per_page' => 4,
					'paged'          => $paged
					
				));
				var_dump("222222222222222222222222");
				var_dump($paged);

				if ( $get_latest_news->have_posts() ) : while ( $get_latest_news->have_posts() ) : $get_latest_news->the_post(); ?>
				<div class="news-summary">

					<div class="news-summary__body">
						<div class="info">
							 
							<h1><?php the_title(); ?></h1>
							<span ><?php echo get_the_date('j', get_the_ID() ); ?> <?php echo get_the_date('M', get_the_ID() ); ?> <?php echo get_the_date('Y', get_the_ID() ); ?></span>
							
						</div>
						<div class="newsbody">

							<div class="page-section__inner__left">



								<?php the_post_thumbnail(); ?>

							</div>

							<div class="page-section__inner__right text">

								 <?php the_excerpt(); ?>
								<a href="<?php the_permalink(); ?>" class="button">Read More</a>
							 </div>

						</div>

					</div>
				</div>
				
				
				<?php endwhile; endif; ?>
				
				<div class="blog__pagination">
					<?php echo custom_pagination($get_latest_news->max_num_pages,4,$paged); ?>
				</div>
				<?php wp_reset_query(); ?>


			</div>

		</section>

<?php

        get_footer();

    endwhile; // end the loop

<?php

function custom_pagination($numpages = '', $pagerange = '', $paged='') {

  if (empty($pagerange)) {
    $pagerange = 2;
  }

  /**
   * paginate.php
   * Used for paginating wp_query posts
   *
   * Sample Usage:
   * <?php $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1; ?>
   * <?php echo custom_pagination($posts->max_num_pages,5,$paged); ?>
   */

  /**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   *
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }

  /**
   * We construct the pagination arguments to enter into our paginate_links
   * function.
   */
  $pagination_args = array(
    'base'            => preg_replace('/\?.*/', '/', get_pagenum_link(1)) . '%_%',
    'format'          => '?paged=%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('&laquo;'),
    'next_text'       => __('&raquo;'),

    'add_args' => array(

     )
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='pagination'>";
      echo $paginate_links;
      ?>

      <a class="paginate paginate--previous">
      </a>
      <a class="paginate paginate--next">
      </a>

      <?php
    echo "</nav>";
  }

}

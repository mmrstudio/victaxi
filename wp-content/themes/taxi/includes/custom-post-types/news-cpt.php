<?php
/**
 * Create our news custom post type
 * @var CPT
 */
$news = new CPT(array(
	'post_type_name' => 'newsarticle',
	'singular' => 'News',
	'plural' => 'News',
	'slug' => 'newsarticle'
	), array(
	'supports' => array(
		'title',
		'editor',
		'thumbnail',
		'excerpt'
	)
));


$news->register_taxonomy(

					array(
						'taxonomy_name' => 'news_types',
				    'singular' => 'News Type',
				    'plural' => 'News Types',
				    'slug' => 'news_types',
					)



);

$news->register_taxonomy(

					array(
						'taxonomy_name' => 'news_state',
				    'singular' => 'State',
				    'plural' => 'States',
				    'slug' => 'news_state',
					)



);




$news->register_taxonomy(


					array(
							'taxonomy_name' => 'tags',
							'singular' => 'Tag',
							'plural'  => 'Tags',
							'slug' => 'tags',
							'hierarchical' => true,
						)

);


/**
 * Set menu icon
 */
$news->menu_icon("dashicons-megaphone");

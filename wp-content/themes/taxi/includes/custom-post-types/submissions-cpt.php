<?php

/**
 * Create custom post type
 * @var CPT
 */
$submissions = new CPT(array(
	'post_type_name' => 'submission',
	'singular' => 'submission',
	'plural' => 'Submissionss',
	'slug' => 'submissions'
	), array(
	'supports' => array(
		'title',
		'editor',
		'thumbnail',
		'excerpt'
	)
));


$submissions->register_taxonomy(

					array(
						'taxonomy_name' => 'group',
				    'singular' => 'Group',
				    'plural' => 'Groups',
				    'slug' => 'groups',
					)



);



$submissions->register_taxonomy(


					array(
							'taxonomy_name' => 'doctags',
							'singular' => 'Tag',
							'plural'  => 'Tags',
							'slug' => 'doctags',
							'hierarchical' => true,
						)

);


/**
 * Set menu icon
 */
$submissions->menu_icon("dashicons-media-document");

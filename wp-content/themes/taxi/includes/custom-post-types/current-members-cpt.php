<?php

    // define cpt
    $cpt = array(
        'post_type_name' => 'member',
        'singular' => 'Current Member',
        'plural' => 'Current Members',
        'slug' => 'members'
    );

    // define cpt options
    $options = array(
        'supports' => array('title', 'editor', 'page-attributes'),
        'rewrite' => array(
            'slug' => 'members',
            'pages' => false
        ),
    );

    // create cpt
    $members_cpt = new CPT($cpt, $options);

    // set dashicon
    $members_cpt->menu_icon('dashicons-admin-users');

    // add state taxonomy
    $members_cpt->register_taxonomy(array(
        'taxonomy_name' => 'state',
        'singular' => 'State',
        'plural' => 'States',
        'slug' => 'state'
    ));




  // get members by state
	function get_members_by_state($state=false) {

  		$categories = array();
          $stockist_state_query = array();

          if($state) :
              $stockist_state_query['slug'] = $state;
          endif;

  		$get_categories = get_terms(array('stockist_state'), $stockist_state_query);

  		foreach($get_categories as $cat) :
  			$categories[$cat->slug] = $cat->name;
  		endforeach;

          //print_r($categories); exit;

  		return $categories;

	}



    // function to retrieve members
    function get_members($state=false) {

        $members = array();

        // define query params
        $members_query = array(
            'post_status' => 'publish',
            'post_type' => 'member',
            'orderby' => 'post_title',
            'order' => 'ASC',
            'posts_per_page' => -1
        );

    		// define tax query
    		if($state) :

    			$members_query['tax_query'] = array(
    				array(
    					'taxonomy' => 'state',
    					'field'    => 'slug',
    					'terms'    => $state,
    				),
    			);

    		endif;

      //  print_r($members_query); exit;

        // query db
        $get_members = new WP_Query($members_query);

		    //var_dump($get_members);

        // get result
        $s = 1;
        if ( $get_members->have_posts() ) : while ( $get_members->have_posts() ) : $get_members->the_post();


            $member_name = get_the_title();
            $member_post_id = get_the_ID();

            $term_list = get_the_terms ($member_post_id,'state');


            $state =  $term_list[0]->{'slug'};
			$state_city =  $term_list[0]->{'description'};
            //die("2222222222222");


        		$member = array(
        		    'id' => $member_post_id,
        		    'name' => get_the_title(),
        		    //'street_address' => get_field('street_address'),
        		    //'suburb' => get_field('suburb'),
        		    'state' => $state,
					'city' => $state_city,
    				    //'state_abbr' => $state_abbr,
        		    //'postcode' => get_field('postcode'),
        		    'phone' => get_field('phone'),
        		    //'opening_hours' => get_field('opening_hours'),
        		    'website' => get_field('website'),
        		    //'email' => get_field('email'),
        		    'lat' => get_field('map_latitude'),
        		    'lon' => get_field('map_longitude'),
        		    'apple_url' => get_field('app_url_apple'),
                'google_url' => get_field('app_url_google_play'),
                'area' => get_field('serving_area'),
                'map_link' => get_field('map_link'),
        		);

            $members[] = $member;

            $s++;

        endwhile; endif;

        wp_reset_query();

        //print_r($members); exit;

        return $members;

    }


    function get_members_json($state=false) {
        $members = get_members($state);
        return json_encode($members);
    }

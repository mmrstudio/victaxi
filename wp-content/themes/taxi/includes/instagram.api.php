<?php

	require_once (TEMPLATEPATH . '/includes/classes/instagram.class.php');
	$insta_access_token = '226910782.6b73596.1a8868416c3d41329484b1beefd20100';
	
	$instagram = new Instagram(array(
		'apiKey'      => '6b73596b00734f1280c5a851ed3bf2af',
		'apiSecret'   => 'b176f1bfab7b45d684eb67757665e036',
		'apiCallback' => 'http://nat-fyfe/'
	));



/*
	if( isset($_GET['code']) ) :
		$code = $_GET['code'];
		print_r($instagram->getOAuthToken($code));
	else :
		echo "<a href='{$instagram->getLoginUrl()}'>Login with Instagram</a>"; exit;
	endif;
	exit;
*/


	
	$instagram->setAccessToken($insta_access_token);
	
	function get_instagram_feed( $limit=10 ) {
	
		global $instagram;
	
		$instagram_images = array();
		
		//delete_transient( 'instagram_images' );

/*
        if ( false === ( $instagram_response = get_transient( 'instagram_response' ) ) ) :
            $instagram_response = $instagram->getUserMedia('self' , $limit);
            set_transient( 'instagram_response' , $instagram_response , 60*60*2 );
        endif;
*/

        $instagram_response = $instagram->getUserMedia('self' , $limit);
		
		if ( false === ( $instagram_images = get_transient( 'instagram_images' ) ) ) :
		
			//print_r($instagram_response); exit;

			if( isset( $instagram_response->data ) ) :
			
				foreach( $instagram_response->data as $image_data ) :

					$image_data->caption->text = removeEmoji($image_data->caption->text);
					//print_r($image_data->images->thumbnail->url); exit;

                    // download and save image locally, resize thumbs
                    $full_image = save_insta_image( $image_data->images->standard_resolution->url );
                    $thumb = resize_insta_image($full_image, 250, 250);
                    $thumb_2x = resize_insta_image($full_image, 300, 300);
				
					$instagram_images[] = array(
						'id' => $image_data->id,
						'date' => date('jS F Y', $image_data->created_time),
						'link' => $image_data->link,
						//'image' => $image_data->images->standard_resolution,
						'full' => insta_image_url( $full_image ),
						'thumb' => insta_image_url( $thumb ),
						'thumb_2x' => insta_image_url( $thumb_2x ),
						//'caption' => $image_data->caption
					);
				
				endforeach;

                //print_r($instagram_images); exit;
			
			endif;	
			
			set_transient( 'instagram_images' , $instagram_images , 60*60*2 );
		
		endif;
		
		//print_r($instagram_images); exit;

		return $instagram_images;
		
	}


    function save_insta_image($url) {
        //preg_match("/.com\/(.*).jpg$/", $url, $matches);
        //$filename = $matches[1].'.jpg';

        $url_parts = explode('/', $url);
        $filename = $url_parts[(count($url_parts)-1)];

        $saveto = insta_image_path($filename);

        //echo $saveto . "\n";

        if( !file_exists($saveto) ) :
            $ch = curl_init ($url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
            $raw=curl_exec($ch);
            curl_close ($ch);
            if(file_exists($saveto)){
                unlink($saveto);
            }
            $fp = fopen($saveto,'x');
            fwrite($fp, $raw);
            fclose($fp);
        endif;

        return $filename;
    }

    function resize_insta_image($filename, $x, $y) {

        $original_file = insta_image_path($filename);
        $resize_image = str_replace('.jpg', '_'.$x.'x'.$y.'.jpg', $filename);
        $resize_image_file = insta_image_path($resize_image);
        //echo $filename."\n";

        if( !file_exists($resize_image_file) ) :
            $image = wp_get_image_editor($original_file);
            if ( ! is_wp_error( $image ) ) :
                $image->resize( $x, $y, true );
                $image->save($resize_image_file);
            else :
                echo $image->get_error_message();
            endif;
        endif;

        return $resize_image;
    }

    function insta_image_path($filename) {
        return WP_CONTENT_DIR . '/instagram/' . $filename;
    }

    function insta_image_url($filename) {
        return WP_CONTENT_URL . '/instagram/' . $filename;
    }


<?php

    /* Template Name: Page -current members */

	  get_header();

		    if ( have_posts() ) while ( have_posts() ) : the_post(); // start loop







		        // get state
		        $state = (isset($_GET['state']) ? $_GET['state'] : false);


		        // get state info
		        switch($state) :
		            case 'vic' : $state_info = array('name' => 'Victoria', 'lat' => -37.479332, 'lon' => 144.983718, 'zoom' => 7); break;
		            case 'nsw' : $state_info = array('name' => 'New South Wales', 'lat' => -32.472695, 'lon' => 146.931152, 'zoom' => 6); break;
		            case 'qld' : $state_info = array('name' => 'Queensland', 'lat' => -21.698265, 'lon' => 146.997070, 'zoom' => 5); break;
		            case 'sa' : $state_info = array('name' => 'South Australia', 'lat' => -31.391158, 'lon' => 135.351563, 'zoom' => 6); break;
		            default : $state_info = array('name' => false, 'lat' => -24.994167, 'lon' => 134.866944, 'zoom' => 4); break;
		        endswitch;



		?>





				<section class="wrapper  first">
						<div class="wrapper__inner" >
								<h1 class="title">Membership</h1>


								<div class="side_menu">
										<ul>
												<?php

													 $partent_id = wp_get_post_parent_id (get_the_ID());
													 $the_query = new WP_Query(
													 array(
														'post_type'      => 'page',
														'posts_per_page' => -1,
														'post_parent' => $partent_id,
														'orderby'        => 'menu_order'
													)
												);

												while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

															<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>

											<?php  endwhile; wp_reset_query(); ?>
									</ul>
								</div>


								<div class="side_menu_right">



									<script type="text/javascript">

										jQuery(document).ready(function($) {

												var members = <?php echo get_members_json($state); ?>;
												var mapPin = '<?php echo get_template_directory_uri() . '/images/map-pin.png'; ?>';
												var mapPinColor = '<?php echo get_template_directory_uri() . '/images/map-pin-color.png'; ?>';
												var mapCenter = new google.maps.LatLng(<?php echo $state_info['lat']; ?>, <?php echo $state_info['lon']; ?>);
												var mapZoom = <?php echo $state_info['zoom']; ?>;

												initializeGoogleMap(members, mapPin, mapPinColor, mapCenter, mapZoom);


										});

									</script>


									<div class="google-search-box-row">
											<h2>Current members</h2>



											<div class="google-search-box">

													<div class="map_discription"><?php the_content(); ?></div>
													<div class="search-box">
														<input type="text" class="input" id="mapAutocomplete" placeholder="State, Suburb or Postcode">
														<button class="search-btn btn btn-orange" id="searchMap">Search</button>

													</div>
											</div>

											<div class="google-map" id="map-canvas"></div>

											<div class="current-members-list" >

											 <div class="members-list-wrap">


													<?php

														$state_list = $state;


														if(!$state_list){$state_list = get_terms('state');}

													  //var_dump	($state_list);

														 foreach($state_list as $state) : ?>

																	<div class="state">

																			<h3 class="state-header"><?php echo $state->{'name'}; ?></h3>




																			<?php
																			$all_area_counter =0;
																			$city_counter = 0;
																			$region_counter = 0;



																			foreach(get_members($state->{'slug'}) as $member) :

																						if (($member['area']=='')||($member['area']=='all')): $all_area_counter ++;
																								elseif($member['area']=='city'): $city_counter ++;
																								elseif($member['area']=='region'): $region_counter ++;
																				  endif; ?>
																			<?php endforeach; ?>


																			<?php if($all_area_counter > 0 ): ?>
																			<h4 class="area all">all</h4>

																				<div class="state__list all">
																				<?php	foreach(get_members($state->{'slug'}) as $member) :

																								if (($member['area']=='')||($member['area']=='all')): ?>
																										<div class="row member-summary  child" data-member-id="<?php echo $member['id']; ?>">




																											<div class="member-col">
																												<h4 class="name"><?php echo $member['name']; ?></h4>


																													<?php if($member['phone']) : ?><span class="phone"><?php echo $member['phone']; ?></span><br><?php endif; ?>
																													<?php if($member['website']) : ?><a class="website" href="http://<?php echo $member['website']; ?>"><?php echo $member['website']; ?></a><br><?php endif; ?>
																													<?php if(($member['apple_url'])||($member['google_url'])) : ?>
																													<span class="download">Download app

																													<?php if($member['apple_url']) : ?><a class="apple_app" href="http://<?php echo $member['apple_url']; ?>"><?php echo $member['apple_url']; ?></a><?php endif; ?>
																													<?php if($member['google_url']) : ?><a class="google_app" href="http://<?php echo $member['google_url']; ?>"><?php echo $member['google_url']; ?></a><br><?php endif; ?>


																													</span>

																													<?php endif; ?>
																											</div>

																										</div>
																							<?php   endif; ?>
																					<?php endforeach; ?>
																			</div>
																			<?php endif; ?>


																			<?php if($city_counter > 0 ): ?>
																			<h4 class="area"><?php echo $state->{'description'}; ?> and surrounds</h4>
																			<div class="state__list city">

																						<?php
																						foreach(get_members($state->{'slug'}) as $member) :

																									if ($member['area']=='city'): ?>
																												<div class="row member-summary  child" data-member-id="<?php echo $member['id']; ?>">




																													<div class="member-col">
																														<h4 class="name"><?php echo $member['name']; ?></h4>


																															<?php if($member['phone']) : ?><span class="phone"><?php echo $member['phone']; ?></span><br><?php endif; ?>
																															<?php if($member['website']) : ?><a class="website" href="http://<?php echo $member['website']; ?>"><?php echo $member['website']; ?></a><br><?php endif; ?>
																															<?php if(($member['apple_url'])||($member['google_url'])) : ?>
																															<span class="download">Download app

																															<?php if($member['apple_url']) : ?><a class="apple_app" href="http://<?php echo $member['apple_url']; ?>"><?php echo $member['apple_url']; ?></a><?php endif; ?>
																															<?php if($member['google_url']) : ?><a class="google_app" href="http://<?php echo $member['google_url']; ?>"><?php echo $member['google_url']; ?></a><br><?php endif; ?>


																															</span>

																															<?php endif; ?>
																													</div>

																												</div>
																									<?php endif; ?>
																						<?php endforeach; ?>
																					</div>
																					<?php endif; ?>

																				<?php if($region_counter > 0 ): ?>
																						<h4 class="area">Regional cities</h4>
																						<div class="state__list region">

																					<?php
																					foreach(get_members($state->{'slug'}) as $member) :

																								if ($member['area']=='region'): ?>
																								<div class="row member-summary  child" data-member-id="<?php echo $member['id']; ?>">




																									<div class="member-col">
																										<h4 class="name"><?php echo $member['name']; ?></h4>


																											<?php if($member['phone']) : ?><span class="phone"><?php echo $member['phone']; ?></span><br><?php endif; ?>
																											<?php if($member['website']) : ?><a class="website" href="http://<?php echo $member['website']; ?>"><?php echo $member['website']; ?></a><br><?php endif; ?>
																											<?php if(($member['apple_url'])||($member['google_url'])) : ?>
																											<span class="download">Download app

																											<?php if($member['apple_url']) : ?><a class="apple_app" href="http://<?php echo $member['apple_url']; ?>"><?php echo $member['apple_url']; ?></a><?php endif; ?>
																											<?php if($member['google_url']) : ?><a class="google_app" href="http://<?php echo $member['google_url']; ?>"><?php echo $member['google_url']; ?></a><br><?php endif; ?>


																											</span>

																											<?php endif; ?>
																									</div>

																								</div>
																								<?php endif; ?>
																					<?php endforeach; ?>
																		</div>
																				<?php endif; ?>
																	</div>

												<?php endforeach;?>

										</div>

								</div>
								</div>

					</div>

				</section>

		<?php



		    endwhile; // end the loop

				get_footer();

<?php
    // Load extra helper functions
    require_once (TEMPLATEPATH . '/includes/helper_functions.php');
	//require_once (TEMPLATEPATH . '/includes/instagram.api.php');
    //require_once (TEMPLATEPATH . '/includes/ajax_functions.php');
 	 require_once (TEMPLATEPATH . '/includes/paginate.php');


    // Set default CONSTANTS
    define( 'THEME_URL' , get_template_directory_uri() );
    define( 'PAGE_BASENAME' , get_current_basename() );
    define( 'FIRST_VISIT' , is_first_time());
	  define( 'HOME_URL' , get_permalink(get_page_by_path('home')) );

    // Load custom post types
    require_once (TEMPLATEPATH . '/includes/classes/CPT.php');
    require_once (TEMPLATEPATH . '/includes/custom-post-types/news-cpt.php');
    require_once (TEMPLATEPATH . '/includes/custom-post-types/current-members-cpt.php');
    require_once (TEMPLATEPATH . '/includes/custom-post-types/submissions-cpt.php');

    if (!is_user_logged_in()) {
        add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
    }

    function ajax_login(){

        // First check the nonce, if it fails the function will break
        check_ajax_referer( 'ajax-login-nonce', 'security' );

        // Nonce is checked, get the POST data and sign user on
        $info = array();
        $info['user_login'] = $_POST['username'];
        $info['user_password'] = $_POST['password'];
        $info['remember'] = true;

        $user_signon = wp_signon( $info, false );
        if ( is_wp_error($user_signon) ){
            echo json_encode(array('loggedin'=>false, 'message'=>__('Wrong username or password.')));
        } else {
            echo json_encode(array('loggedin'=>true, 'message'=>__('Login successful, redirecting...')));
        }

        die();
    }





    // Add theme supports
    add_theme_support('post-thumbnails');

    // Register navigation
    register_nav_menu( 'main-nav' , 'Main Menu' );

    // Add image sizes
    add_image_size('small', 500 , 0 , false );

    add_image_size( 'thumb', 500, 360, true);


    add_image_size( 'docs', 540, 690, true);

    // Set default image link type
    update_option('image_default_link_type', 'file');

    // Set JPG quality for image resizing
    add_filter( 'jpeg_quality', 'set_jpg_quality' );
    function set_jpg_quality( $quality ) { return 95; }

	add_filter( 'redirect_canonical','custom_disable_redirect_canonical' );
	function custom_disable_redirect_canonical( $redirect_url ){
		if ( is_singular('news') ) $redirect_url = false;
		return $redirect_url;
	}


    // Load stylesheets and javascript files
    add_action('init', 'theme_init');

    // Initilization function
    function theme_init() {

        // Load stylesheets and javascript files
        load_scripts_and_styles();

        // Create options page
        create_theme_options_page();

        // hide stuff we don't need
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'wp_generator');
        remove_action('wp_head', 'index_rel_link');
        remove_action('wp_head', 'feed_links');

    }


    function load_scripts_and_styles() {

        if( ! is_admin() ) :

            // Add stylesheets
            wp_enqueue_style( 'main_styles' , THEME_URL . '/style.css' , FALSE , '0.2.0' , 'screen' );

            // Load scripts
            add_action('wp_enqueue_scripts', 'load_scripts', 0);

            // Replace jQuery with Google CDN version
            function load_scripts() {

                // Load jQuery from CDN
                $jquery_version = '3.2.1';
                wp_deregister_script( 'jquery' );
                wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/' . $jquery_version . '/jquery.min.js' , array() , $jquery_version , FALSE );
                wp_enqueue_script( 'jquery' );






                // Load modernizr
                //wp_enqueue_script( 'modernizr' , THEME_URL . '/js/vendor/modernizr.min.js' , '' , '0.1.0' , FALSE );

                // Load main scripts
                wp_enqueue_script( 'main_script' , THEME_URL . '/js/main.js' , 'jquery' , '0.2.0' , TRUE );


            }

        endif;

    }

    function create_theme_options_page() {

        if( function_exists('acf_add_options_page') ) {

        	$page = acf_add_options_page(array(
        		'page_title' 	=> 'Options',
        		'menu_title' 	=> 'Options',
        		'menu_slug' 	=> 'options',
        		'capability' 	=> 'edit_posts',
        		'redirect' 	=> false
        	));

        }

    }

    // Increase excerpt length
    add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
    function custom_excerpt_length( $length ) { return 100; }

    // Filter HTML in Wordpress text widget
    add_filter('widget_text', 'filter_text_widget');
    function filter_text_widget( $text ) { return apply_filters('the_content', $text); }

    // Remove hardcoded width/height attrs from post thumbnails
    add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10, 3 );
    function remove_thumbnail_dimensions( $html, $post_id, $post_image_id ) {
        $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
        return $html;
    }

    // Remove dimension attributes from img tags in content
    add_filter('the_content', 'remove_img_dimensions', 10);
    function remove_img_dimensions($html) {
        $html = preg_replace('/(width|height)=["\']\d*["\']\s?/', "", $html);
        return $html;
    }

    // Add nav menu item names to links
    add_filter( 'nav_menu_css_class', 'add_nav_item_name_class', 10, 2 );
    function add_nav_item_name_class( $classes , $item ) {
        $new_class = strtolower( preg_replace("/[^A-Za-z0-9 ]/", '-', $item->title) );
        $new_class = str_replace(' ', '-', $new_class);
        $classes[] = $new_class;
        $classes[] = $new_class.'-link';
        return $classes;
    }

/*
    // wp-admin login screen logo
    add_action('login_enqueue_scripts', 'admin_login_logo');
    function admin_login_logo() {
        echo '<style> .login #backtoblog a, .login #nav a { color: #000 !important; } .login h1 a { background: url('.THEME_URL.'/images/login-logo.png) no-repeat center center transparent; background-size: auto auto; width: 320px; } </style>'."\n";
    }
*/

    add_filter( 'body_class', 'add_slug_body_class' );
    function add_slug_body_class( $classes ) {
        global $post;
        if (isset($post)) $classes[] = $post->post_type . '-' . $post->post_name;
        return $classes;
    }

    add_filter('show_admin_bar', '__return_false');

/*
    add_filter('acf/settings/show_admin', 'my_acf_show_admin');
    function my_acf_show_admin( $show ) {
        return current_user_can('manage_capabilities');
    }
*/

    //add_filter("gform_submit_button", "custom_gform_button", 10, 2);
    function custom_gform_button($button, $form){
        return "<button class='button' id='gform_submit_button_{$form["id"]}'>Submit</button>";
    }

    add_filter( 'gform_confirmation_anchor', '__return_false' );


	function cc_mime_types($mimes) {
	  $mimes['svg'] = 'image/svg+xml';
	  return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');

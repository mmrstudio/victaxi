<?php

    /* Template Name: Page - Contact Us */

		get_header();

		if(have_posts()) while (have_posts()) : the_post();



?>

<section class="wrapper first passengers">
	<div class="wrapper__inner" >
			<h1 class="title"><?php the_title(); ?></h1>
			<div class="content">
				<?php the_content(); ?>

			</div>
			<div class="contact">
						<?php

						// check if the repeater field has rows of data
						if( have_rows('contact') ):

						// loop through the rows of data
						while ( have_rows('contact') ) : the_row(); ?>


						<div class="contact__single">
								<h3><?php the_sub_field('name'); ?></h3>

								<?php

											if( have_rows('contact_person') ):

											// loop through the rows of data
													while ( have_rows('contact_person') ) : the_row();

															 if(get_sub_field('contact_name')):
																	echo '<div class="contact_name">'.get_sub_field('contact_name').'</div>';
															 endif;

															 if(get_sub_field('title')):
																	echo '<div class="title">'.get_sub_field('title').'</div>';
															 endif;

													endwhile;

											else :

											// no rows found

											endif;

											if (get_sub_field('address')):
												echo '<div class="add">'.get_sub_field('address').'</div>';
											endif;

											if(get_sub_field('phone')):
													echo '<div class="phone">'.get_sub_field('phone').'</div>';
											 endif;

											 if(get_sub_field('fax')):
 													echo '<div class="fax">'.get_sub_field('fax').'</div>';
 											 endif;

											 if(get_sub_field('mobile')):
													 echo '<div class="mobile">'.get_sub_field('mobile').'</div>';
											 endif;

											 if(get_sub_field('email')):
 													echo '<div class="email">'.get_sub_field('email').'</div>';
 											 endif;



											 if(get_sub_field('web')):
 													echo '<div class="web">'.get_sub_field('web').'</div>';
 											 endif;


								 ?>
						</div>
						<?php endwhile;

						else :

						// no rows found

						endif;

						?>
			</div>

	</div>
</section>


<?php

		endwhile; // end loop

		get_footer();
